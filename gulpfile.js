var gulp = require('gulp'),
    livereload = require('gulp-livereload'),
    //uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    connect = require( 'gulp-connect'),
    files = [ 'index.html', 'styles/style.css'],
    sass = require('gulp-sass');
    //lr = require('tiny-lr'),
    //server = lr(),
    //connect = require('connect');

gulp.task( 'files', function() {
    gulp.src( files ).pipe( connect.reload() );
});

gulp.task( 'watch', function() {
    gulp.watch( files, [ 'files' ]);
});

gulp.task( 'connect', function() {
    connect.server({ livereload: true });
});

gulp.task('styles', function() {
    gulp.src('styles/*.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('styles/css'))
});



//Watch task
gulp.task('watch_sass',function() {
    gulp.watch('styles/*.sass',['styles']);
});

gulp.task( 'default', [ 'connect', 'watch_sass' ]);